function[monoChannel] = FilterApB(fmSignal, cFilterMono, cDownSamplingRate)

	%load filter 
	load(cFilterMono);
	Den = [1,zeros(1,length(Num)-1)];

	%filter signal 
	filteredMono = filter(Num, Den, fmSignal);
	monoChannel = downsample(filteredMono,cDownSamplingRate);
end