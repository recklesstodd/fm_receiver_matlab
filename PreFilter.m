function[fmSignal] = PreFilter(demodSignal, cPrefilterName, cPreSamplingFrequency)

	load(cPrefilterName);

	Den = [1,zeros(1,length(Num)-1)];

	fmSignal = filter(Num, Den, demodSignal);

	fmSignal= downsample(fmSignal,1/cPreSamplingFrequency);
end