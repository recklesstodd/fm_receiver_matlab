function[y] = pll_BP(fmSignal,Fc,Fs)

	load('FIR_pll.mat');
	x = fmSignal+1j*fmSignal;
	n = length(x);
	y = zeros(n,1)+1j*zeros(n,1);
	phd_output = zeros(n,1);
	phi_hat = zeros(n,1);
	
	Order = length(Num);
	
	for ind=(Order+1:1:n)
		
		%calculate output
		y(ind) = conj(exp(j*(2*pi*(Fc/Fs)*ind + phi_hat(ind-1))));
		
		% Compute error estimate
		phd_output(ind) = imag(x(ind)*y(ind));	
		for filt_ind = 1:Order;
			phi_hat(ind) = phi_hat(ind) + Num(filt_ind) * phd_output(ind - filt_ind);
		end
			
		
	end

	y=real(y);
end



