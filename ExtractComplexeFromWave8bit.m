function [complexRawSignal] = ExtractComplexeFromWave8bit(cFileName)
	sourceFID = fopen(cFileName,'r');

	%Read samples and stores them in 8 bitfomat
	rawSignal = fread(sourceFID);


	%Remove the offset 
	rawSignal = rawSignal - 127.5;

	%Normalise to +/-128
	rawSignal = (rawSignal/max(abs(rawSignal)))*127;

	%Reconstruct complexe samples
	complexRawSignal = rawSignal(1:2:end-1)+1j*rawSignal(2:2:end);

	%Clean
	clear rawSignal;
	fclose(sourceFID);
end