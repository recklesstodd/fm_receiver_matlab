clear all, close all, clc

%Constant values%

cFileName = 'f94_4s200khzg20.bin';
cPrefilterName = 'FIR_Hamming_0.5Fc.mat';
cFilterBandPass ='FIR_Hamming_Notch_0.3.mat';
cFilterMono = 'FIR_Hamming_0.24Fc.mat';


cSignalDuration = 10;    	% in seconds
cSamplingRate = 250000; 	% in samples/second
cBandWidth = cSamplingRate / 2;
cPreSamplingFrequency = 0.5;
cFFTSize = 1024*16;
cDisplayFft = 0;
cApbDownsamplingRate = 4;


modSignal = ExtractComplexeFromWave8bit(cFileName);
demodSignal = FMDemod(modSignal);

if (cDisplayFft == 1)
	fftDemodSignal = abs(fftshift(fft(demodSignal,cFFTSize)));
	xaxis = linspace(-cBandWidth,cBandWidth-cBandWidth/cFFTSize,cFFTSize);
	subplot(2,2,1)
	plot(xaxis,fftDemodSignal);
	title('Demodulated samples in frequency domain');
	xlabel('Frequency');
	ylabel('Magnitude');
end

fmSignal = PreFilter(demodSignal, cPrefilterName, cPreSamplingFrequency);

if (cDisplayFft == 1)
	fftFmSignal = abs(fftshift(fft(fmSignal,cFFTSize)));
	fmSignalBandWidth = cBandWidth * cPreSamplingFrequency;
	xaxis = linspace(-fmSignalBandWidth,fmSignalBandWidth-fmSignalBandWidth/cFFTSize,cFFTSize);
	subplot(2,2,2)
	plot(xaxis,fftFmSignal);
	title('Prefiltred samples in frequency domain');
	xlabel('Frequency');
	ylabel('Magnitude');
end

% Filter A+B channel 
apbChannel = FilterApB(fmSignal, cFilterMono, cApbDownsamplingRate);
if (cDisplayFft == 1)
	fftFmSignal = abs(fftshift(fft(apbChannel,cFFTSize)));
	fmSignalBandWidth = cBandWidth * cPreSamplingFrequency * (1/cApbDownsamplingRate);
	xaxis = linspace(-fmSignalBandWidth,fmSignalBandWidth-fmSignalBandWidth/cFFTSize,cFFTSize);
	subplot(2,2,3)
	plot(xaxis,fftFmSignal);
	title('Mono signal');
	xlabel('Frequency');
	ylabel('Magnitude');
end



%Extraction porteuse à 19 khz
carrierSignal = ExtractCarrier(fmSignal, cFilterBandPass);
if (cDisplayFft == 1)
	fftFmSignal = abs(fftshift(fft(carrierSignal,cFFTSize)));
	fmSignalBandWidth = cBandWidth * cPreSamplingFrequency;
	xaxis = linspace(-fmSignalBandWidth,fmSignalBandWidth-fmSignalBandWidth/cFFTSize,cFFTSize);
	subplot(2,2,4)
	plot(xaxis,fftFmSignal);
	title('Mono signal');
	xlabel('Carrier frequency');
	ylabel('Magnitude');
end

carrierSignalPll = pll_BP(fmSignal,19000,125000);


xaxis = 1:1:length(carrierSignal);

plot(xaxis,carrierSignal,xaxis,carrierSignalPll);
legend('a','b')
