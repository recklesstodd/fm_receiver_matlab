function [demodSignal] = FMDemod(modSignal)

	%Time shift
	modSignal1 = modSignal(1:end-1);
	modSignal2 = modSignal(2:end);

	%Demodulation FM
	Signal = modSignal1 .* conj(modSignal2);
	demodSignal = angle(Signal);
end