function[carrierSignal] = ExtractCarrier(fmSignal, cFilterBandPass)

	load(cFilterBandPass);

	Den = [1,zeros(1,length(Num)-1)];

	%filter signal 
	carrierSignal = filter(Num, Den, fmSignal);
end